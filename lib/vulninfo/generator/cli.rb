# frozen_string_literal: true

require 'thor'

module Vulninfo
  module Generator
    # Handle the application command line parsing
    # and the dispatch to various command objects
    #
    # @api public
    class CLI < Thor
      # Error raised by this runner
      Error = Class.new(StandardError)

      desc 'version', 'vulninfo-generator version'
      def version
        require_relative 'version'
        puts "v#{Vulninfo::Generator::VERSION}"
      end
      map %w[--version -v] => :version

      desc 'generate INFILE', 'generate vulninfo from IN'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'

      def generate(infile)
        if options[:help]
          invoke :help, ['generate']
        else
          require_relative 'commands/generate'
          Vulninfo::Generator::Commands::Generate.new(infile, options).execute
        end
      end
    end
  end
end
