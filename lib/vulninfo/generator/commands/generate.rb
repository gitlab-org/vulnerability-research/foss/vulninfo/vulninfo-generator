# frozen_string_literal: true

require_relative '../command'
require_relative '../ontology'
require 'nokogiri'

module Vulninfo
  module Generator
    module Commands
      class CweWalker < Nokogiri::XML::SAX::Document
        attr_accessor :vulninfo, :text

        def initialize
          @context = []
          @taxonomies = {}
          @vulninfo = Ontology.new
          @text = StringIO.new
          @xhtml = []
        end

        def characters(string)
          @text << string.gsub("\n", ' ').gsub(/\t/, ' ').gsub(/ +/, ' ').strip
        end

        def id(sid)
          sid.downcase.gsub(' ', '_')
        end

        def relationhash(r)
          "#{r.source_id}:#{RelationKind.label(r.kind)}:#{r.target_id}"
        end

        def start_element(name, attrs = [])
          case name
          when 'Category'
            attrs.each do |k, v|
              case k
              when 'Name'
                if v.downcase.include?('owasp')
                  pfx, sfx = v.gsub(' Top Ten', '').gsub(' Category', '').split('-')

                  pfx = pfx.strip.downcase.gsub(' ', '-')
                  pfx = pfx.split(':').first.gsub('a0', 'a')

                  sfx = sfx.strip

                  @owasp_mapping = VulnInfoNode.new(NodeKind::OWASP)
                  @owasp_mapping.identifier = pfx
                  @owasp_mapping.description = sfx
                  @owasp_mapping.title = v.gsub(/ Category A.*/, '').split('-').first.strip
                  @vulninfo.nodes[@owasp_mapping.identifier] = @owasp_mapping
                end
              end
            end
          when 'Has_Member'
            unless @owasp_mapping.nil?
              attrs.each do |k, v|
                case k
                when 'CWE_ID'
                  @vulninfo.nodes[@owasp_mapping.identifier] = @owasp_mapping
                  parent = Relation.new
                  parent.source_id = "cwe-#{v}"
                  parent.target_id = @owasp_mapping.identifier
                  parent.kind = RelationKind::MAPS_TO
                  @vulninfo.relations[relationhash(parent)] = parent
                end
              end
            end
            cwecat = nil
          when 'Has_Member'
            attrs.each do |k, v|
              case k
              when 'CWE_ID'
                unless @context.empty? || @context.last.nil? || @context.last.kind != NodeKind::OWASP
                  owasp = @context.last
                  rel = Relation.new
                  rel.source_id = owasp.identifier
                  rel.target_id = "cwe-#{v}"
                  rel.kind = RelationKind::MAPS_TO
                  @vulninfo.relations[relationhash(rel)] = rel
                end
              end
            end
          when 'Description'
            @text = StringIO.new
          when 'Related_Weakness'
            cur_weakness = @context.last
            relation = Relation.new
            relation.source_id = cur_weakness.identifier

            attrs.each do |k, v|
              case k
              when 'Nature'
                case v
                when 'ChildOf'
                  relation.kind = RelationKind::CHILD_OF
                when 'CanPrecede'
                  relation.kind = RelationKind::CAN_PRECEDE
                when 'CanAlsoBe'
                  relation.kind = RelationKind::CAN_ALSO_BE
                when 'PeerOf'
                  relation.kind = RelationKind::PEER_OF
                when 'Requires'
                  relation.kind = RelationKind::REQUIRES
                when 'StartsWith'
                  relation.kind = RelationKind::REQUIRES
                when 'FollowedBy'
                  relation.kind = RelationKind::REQUIRES
                end
              when 'CWE_ID'
                relation.target_id = id("cwe-#{v}")
              end
            end

            if relation.kind == RelationKind::CHILD_OF
              parent = Relation.new
              parent.target_id = relation.source_id
              parent.source_id = relation.target_id
              parent.kind = RelationKind::PARENT_OF
              @vulninfo.relations[relationhash(parent)] = parent
            end
            @vulninfo.relations[relationhash(relation)] = relation
          when 'Weakness'
            weakness = VulnInfoNode.new(NodeKind::WEAKNESS)
            attrs.each do |k, v|
              case k
              when 'ID'
                weakness.identifier = id("CWE-#{v}")
              when 'Name'
                weakness.title = v
              end
            end
            @vulninfo.nodes[weakness.identifier] = weakness
            @context << weakness
          when 'Taxonomy_Mapping'
            attrs.each do |k, v|
              case k
              when 'Taxonomy_Name'
                if v.downcase.include?('owasp')
                  @owasp_mapping = VulnInfoNode.new(NodeKind::OWASP)
                  @owasp_mapping.title = v
                end
              end
            end
          when 'Entry_ID', 'Entry_Name', 'Mapping_Fit'
            @text = StringIO.new
          when 'Potential_Mitigations',
            'Observed_Examples',
            'Detection_Methods',
            'Alternate_Terms'
            @context << nil
          end
        end

        def end_element(name)
          case name
          when 'Category'
            @owasp_mapping = nil
          when 'Description'
            @context.last.description = @text.string if @context.size == 1
            @text = StringIO.new
          when 'Entry_ID'
            unless @owasp_mapping.nil?
              id = "#{@owasp_mapping.title.gsub(/Top Ten /, '').downcase.gsub(' ', '-')}-#{@text.string}"
              id = id.downcase
              @owasp_mapping.identifier = id
            end
          when 'Entry_Name'
            if !@owasp_mapping.nil? && !@context.empty?
              @owasp_mapping.description = @text.string
              @vulninfo.nodes[@owasp_mapping.identifier] = @owasp_mapping
              last = @context.last
              parent = Relation.new
              parent.source_id = last.identifier
              parent.target_id = @owasp_mapping.identifier
              parent.kind = RelationKind::MAPS_TO
              @vulninfo.relations[relationhash(parent)] = parent
              @owasp_mapping = nil
            end
          when 'Weakness',
            'Potential_Mitigations',
            'Observed_Examples',
            'Detection_Methods',
            'Alternate_Terms',
            'Category'
            @context.pop
          end
        end

        def end_document; end
      end

      class Generate < Vulninfo::Generator::Command
        def initialize(infile, options)
          @options = options
          @infile = infile
        end

        def execute(input: $stdin, output: $stdout)
          walker = CweWalker.new
          parser = Nokogiri::XML::SAX::Parser.new(walker)
          parser.parse(File.open(@infile))

          puts "writing #{@infile}.proto"
          File.open("#{@infile}.go", 'wb') do |out|
            out.write(walker.vulninfo.go)
          end
          File.open("#{@infile}.dot", 'wb') do |out|
            out.write(walker.vulninfo.dot)
          end
          File.open("#{@infile}.rb", 'wb') do |out|
            out.write(walker.vulninfo.ruby)
          end
        end
      end
    end
  end
end
