require 'erb'
require 'json'

class String
  def clean
    gsub(/[-.():]/, '_').delete(' ')
  end
end

class Ontology
  attr_accessor :nodes, :relations, :nodemap

  def initialize
    @nodes = {}
    @relations = {}
  end

  def add_node(node)
    @nodes[node.identifier] = node
  end

  def dot
    s = StringIO.new
    s << "digraph {\n"
    s << 'rankdir LR;'
    s << "graph [fontname=\"helvetica\"];\n"
    s << "node [fontname=\"helvetica\",shape=\"rectangle\"];\n"
    s << "edge [fontname = \"helvetica\"];\n"

    @nodes.each do |_k, v|
      s << v.dot
      s << "\n"
    end
    @relations.each do |_k, v|
      s << v.dot
    end
    s << '}'
    s.string
  end

  def ruby
    description = ERB.new(<<~TEMPLATE, nil, '-')
            module RelationKind
              UNKNOWN = 1
              CHILD_OF = 2
              CAN_PRECEDE = 3
              CAN_ALSO_BE = 4
              MAPS_TO = 5
              PARENT_OF = 6
              PEER_OF = 7
              REQUIRES = 8
              STARTS_WITH = 9
              FOLLOWED_BY = 10
            end
      #{'        '}
            module Ontology
      #{'      '}
              def self.nodes
                @@nods
              end
      #{'      '}
              def self.relations
                @@rels
              end
      #{'      '}
              def self.in_relations
                @@in_rels
              end
      #{'      '}
              def self.out_relations
                @@out_rels
              end
      #{'      '}
              @@nods = {
              <% nodes.each_with_index do |ele, idx| %>
                "<%= ele[0] %>" => {
                  identifier: "<%= ele[1].identifier %>",
                  title: <%= ele[1].title.to_json %>,
                  description: <%= ele[1].description.to_json %>,
                  kind: <%= ele[1].kind %>
                }<% if idx < nodes.size - 1 %>,<% end %>
              <% end %>
              }.freeze
      #{'      '}
              @@rels = {
              <% relations.each_with_index do |ele, ridx| %>
                "<%= ele[0] %>" => {
                  identifier: "<%= ele[0] %>",
                  sourceid: "<%= ele[1].source_id %>",
                  targetid: "<%= ele[1].target_id %>",
                  label: "<%= RelationKind.label(ele[1].kind) %>",
                  kind: <%= ele[1].kind %>
                }<% if ridx < relations.size - 1 %>,<% end %>
              <% end %>
              }.freeze
      #{'      '}
              @@out_rels = {
              <% nodes.each_with_index do |ele, nidx| %>
              "<%= ele[0] -%>" => [
                <% relations.each_with_index do |rel, ridx| -%>
                <% if ele[0] == rel[1].source_id -%>
                  "<%= rel[0] %>"<% if ridx < relations.size - 1 %>,<% end %>
                <% end -%>
                <% end -%>
                ]<% if nidx < nodes.size - 1 %>,<% end %>
              <% end -%>
              }.freeze
      #{'      '}
              @@in_rels = {
              <% nodes.each_with_index do |ele, nidx| %>
              "<%= ele[0] -%>" => [
                <% relations.each_with_index do |rel, ridx| -%>
                <% if ele[0] == rel[1].target_id -%>
                  "<%= rel[0] %>"<% if ridx < relations.size - 1 %>,<% end %>
                <% end -%>
                <% end -%>
                ]<% if nidx < nodes.size - 1 %>,<% end %>
              <% end -%>
              }.freeze
                end
    TEMPLATE
    description.result_with_hash(nodes: nodes, relations: relations).to_s
  end

  def go
    description = ERB.new(<<~TEMPLATE, nil, '-')
            package main
      #{'      '}
            import (
              "fmt"
              "strings"
            )
      #{'      '}
            type Ontology struct {
                nodes map[string]*VulnInfoNode
                relations map[string]Relation
                inRelations map[string][]string
                outRelations map[string][]string
            }
      #{'      '}
            func (o *Ontology) CWENodeById(identifier int) (*VulnInfoNode, error) {
                return o.NodeById(fmt.Sprintf("cwe-%d", identifier))
            }
      #{'      '}
            func (o *Ontology) NodeById(identifier string) (*VulnInfoNode, error) {
                node, ok := o.nodes[identifier]
                if !ok {
                    return nil, fmt.Errorf("could not find node: %s", identifier)
                }
                return node, nil
            }
      #{'      '}
            func (o *Ontology) InRelations(identifier string) ([]string, error) {
                inrelation, ok := o.inRelations[identifier]
                if !ok {
                    return nil, fmt.Errorf("could not find relation: %s", identifier)
                }
                return inrelation, nil
            }
      #{'      '}
            func (o *Ontology) OutRelations(identifier string) ([]string, error) {
                outrelation, ok := o.outRelations[identifier]
                if !ok {
                    return nil, fmt.Errorf("could not find relation: %s", identifier)
                }
                return outrelation, nil
            }
      #{'      '}
            func (o *Ontology) Relation(identifier string) (*Relation, error) {
                relation, ok := o.relations[identifier]
                if !ok {
                    return nil, fmt.Errorf("could not find relation: %s", identifier)
                }
                return &relation, nil
            }
      #{'      '}
            func (n *VulnInfoNode) CWEIdentifier() string {
                return strings.ToUpper(n.Identifier)
            }
      #{'      '}
            type VulnInfoKind int
            const (
                CWE = 1
                OWASP = 2
                TAXONOMY_MAPPING = 3
                WEAKNESS = 4
            )
      #{'      '}
            type VulnInfoNode struct {
                Identifier string
                Title string
                Description string
                Kind VulnInfoKind
            }
      #{'      '}
            type RelationKind int
            const (
                UNKNOWN = 1
                CHILD_OF = 2
                CAN_PRECEDE = 3
                CAN_ALSO_BE = 4
                MAPS_TO = 5
                PARENT_OF = 6
                PEER_OF = 7
                REQUIRES = 8
                STARTS_WITH = 9
                FOLLOWED_BY = 10
            )
      #{'      '}
            type Relation struct {
                Identifier string
                SourceId string
                TargetId string
                Label string
                Kind RelationKind
            }
      #{'      '}
            var VulnInfo = Ontology {
                nodes : map[string]*VulnInfoNode {
            <% nodes.each do |key, value| %>
                     "<%= key %>": {
                        Identifier: "<%= value.identifier %>",
                        Title: `<%= value.title %>`,
                        Description: `<%= value.description %>`,
                        Kind: <%= value.kind %>,
                    },
            <% end %>
                },
                relations: map[string]Relation {
            <% relations.each do |key,value| %>
                    "<%= key %>": {
                          Identifier: "<%= key %>",
                          SourceId: "<%= value.source_id %>",
                          TargetId: "<%= value.target_id %>",
                          Label: "<%= RelationKind.label(value.kind) %>",
                          Kind: <%= value.kind %>,
                      },
            <% end %>
                },
                outRelations: map[string][]string {
                <% nodes.each do |nkey, nvalue| %>
                "<%= nkey %>": {
                  <% relations.each do |rkey,rvalue| -%>
                  <% if nkey == rvalue.source_id -%>
                  "<%= rkey %>",
                  <% end -%>
                  <% end -%>
                },
                <% end -%>
              },
                inRelations: map[string][]string {
                <% nodes.each do |nkey, nvalue| %>
                "<%= nkey %>": {
                  <% relations.each do |rkey,rvalue| -%>
                  <% if nkey == rvalue.target_id -%>
                  "<%= rkey %>",
                  <% end -%>
                  <% end -%>
                },
                <% end -%>
              },
            }
    TEMPLATE

    description.result_with_hash(nodes: nodes, relations: relations).to_s
  end
end

module NodeKind
  UNKNOWN = 1
  CWE = 2
  OWASP = 3
  TAXONOMY_MAPPING = 4
  WEAKNESS = 5

  def self.label(id)
    case id
    when CWE
      'cwe'
    when OWASP
      'owasp'
    when TAXONOMY_MAPPING
      'taxonomy'
    else
      'weakness'
    end
  end
end

class VulnInfoNode
  attr_accessor :identifier, :title, :description, :kind, :properties

  def initialize(kind = NodeKind::UNKNOWN, identifier = '', title = '', description = '', properties = {})
    self.identifier = identifier
    self.title = title
    self.description = description
    self.kind = kind
    self.properties = properties
  end

  def dot
    s = StringIO.new
    s << "n#{identifier.clean}[label=\"#{identifier}\"];"
    s.string
  end
end

module RelationKind
  UNKNOWN = 1
  CHILD_OF = 2
  CAN_PRECEDE = 3
  CAN_ALSO_BE = 4
  MAPS_TO = 5
  PARENT_OF = 6
  PEER_OF = 7
  REQUIRES = 8
  STARTS_WITH = 9
  FOLLOWED_BY = 10

  def self.color(id)
    case id
    when UNKNOWN
      'yellow'
    when CHILD_OF
      'green'
    when CAN_PRECEDE
      'purple'
    when CAN_ALSO_BE
      'orange'
    when MAPS_TO
      'blue'
    when PARENT_OF
      'green'
    when PEER_OF
      'beige'
    when REQUIRES
      'aquamarine4'
    when STARTS_WITH
      'coral1'
    when FOLLOWED_BY
      'azure3'
    else
      'black'
    end
  end

  def self.label(id)
    case id
    when UNKNOWN
      'unknown'
    when CHILD_OF
      'child_of'
    when CAN_PRECEDE
      'can_precede'
    when CAN_ALSO_BE
      'can_also_be'
    when MAPS_TO
      'maps_to'
    when PARENT_OF
      'parent_of'
    when PEER_OF
      'peer_of'
    when REQUIRES
      'reqiures'
    when STARTS_WITH
      'starts_with'
    when FOLLOWED_BY
      'followed_by'
    else
      'maps_to'
    end
  end
end

class Relation
  attr_accessor :source_id, :target_id, :kind, :properties

  def initialize(kind = RelationKind::UNKNOWN, source_id = -1, target_id = -1, properties = {})
    @source_id = source_id
    @target_id = target_id
    @kind = kind
    @properties = properties
  end

  def dot
    return '' if @kind == RelationKind::UNKNOWN

    s = StringIO.new
    s << "n#{source_id.clean}->n#{target_id.clean}[color=\"#{RelationKind.color(kind)}\"];\n"
    s.string
  end
end
