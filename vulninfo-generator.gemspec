require_relative 'lib/vulninfo/generator/version'

Gem::Specification.new do |spec|
  spec.name          = 'vulninfo-generator'
  spec.license       = 'MIT'
  spec.version       = Vulninfo::Generator::VERSION
  spec.authors       = ['Isaac Dawson', 'Julian Thome']
  spec.email         = ['idawson@gitlab.com', 'jthome@gitlab.com']

  spec.summary       = 'Generate language bindings from CWE ontology.'
  spec.description   = 'Generate language bindings from CWE ontology.'
  spec.homepage      = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-generator'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = ''
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-generator'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-generator/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'solargraph'
  spec.add_dependency 'logging'
  spec.add_dependency 'nokogiri'
end
