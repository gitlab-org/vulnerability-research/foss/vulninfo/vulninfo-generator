# vulninfo-generator

Vulninfo generator generates language bindings from a CWE dictionary in XML
format. At the moment we support ruby and go as target languages.

## Usage

``` bash
# build vulninfo generator
bundle install
# download latest version of the cwe dictionary
wget https://cwe.mitre.org/data/xml/cwec_latest.xml.zip
unzip cwec_latest.xml.zip
# generates ruby (.rb), go (.go) and dot (.dot) files
./exe/vulninfo-generator generate cwec_v4.4.xml
```

After the execution, you should be able to see `.go`, `.rb` and `.proto` file
in the target directory. You can format the `rb` file using `rufo`.

## License 

vulninfo-generator is licensed used under the [MIT license](LICENSE.txt).
