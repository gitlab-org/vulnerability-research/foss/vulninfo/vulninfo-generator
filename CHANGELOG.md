## vulninfo-generator

## [1.5.0] - 2023-03-12

- More OWASP mappings (!5)

## [1.4.1] - 2021-09-23

- Fix owasp/cwe format (!3)

## [1.4.0] - 2021-09-23

- OWASP categories (!2)

## [1.3.0] - 2021-09-07

- CWE id formatting

## [1.2.0] - 2021-09-07

- Add CWE search function

## [1.1.0] - 2021-09-07

- Extended Ontology generator

## [1.0.0] - 2021-06-16

- Initial release

