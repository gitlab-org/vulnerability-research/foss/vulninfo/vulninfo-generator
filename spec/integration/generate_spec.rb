RSpec.describe '`vulninfo-generator generate` command', type: :cli do
  it 'executes `vulninfo-generator help generate` command successfully' do
    output = `vulninfo-generator help generate`
    expected_output = <<~OUT
Usage:
  vulninfo-generator generate INFILE

Options:
  -h, [--help], [--no-help]  # Display usage information

generate vulninfo from IN
    OUT


    expect(output).to eq(expected_output)
  end
end
